 terraform {
   required_providers {
     yandex = {
       source = "yandex-cloud/yandex"
     }
   }
 }

 provider "yandex" {
   token  =  var.yandex_token
   cloud_id  = var.yandex_cloud_id
   folder_id = var.yandex_folder_id
   zone      = "ru-central1-a"
 }

resource "yandex_vpc_network" "network_terraform" {
  name = "network_terraform"
}

resource "yandex_vpc_subnet" "subnet_terraform" {
  name           = "subnet_terraform"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network_terraform.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  
}

data "yandex_compute_image" "my_image" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "vm-1" {
  name = "server"
  allow_stopping_for_update = true
  platform_id = "standard-v1"
  zone = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my_image.id}"
      size = 32
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
  

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py",
      "python3 get-pip.py --user",
      "python3 -m pip install --user ansible",
      "sudo apt install ansible ca-certificates curl git gnupg lsb-release unzip -y",
      "git clone https://gitlab.com/ask555l1/scripts.git",
      "cd scripts",
      "ansible-playbook -b -c local -i localhost, docker_conf.yml -vv",
#      "ansible-playbook -b -c local -i localhost, run_docker.yml -vv",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "file" {
    source      = "secret"
    destination = "/home/ubuntu/scripts/secret"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "remote-exec" {
    inline = [
        "cd scripts",
        "ansible-playbook -b -c local -i localhost, regi.yml -vv",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = self.network_interface[0].nat_ip_address
    }
  }

  scheduling_policy {
    preemptible = true
  }  
}


resource "yandex_lb_network_load_balancer" "lb-tera" {
  name = "lb-tera"

  listener {
    name = "listener-web-servers"
    port = 80
    target_port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = yandex_vpc_subnet.subnet_terraform.id
    address   = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

#output "internal_ip_address_vm_2" {
#  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
#}

#output "external_ip_address_vm_2" {
#  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
#}

#output "internal_ip_address_vm_3" {
#  value = yandex_compute_instance.vm-3.network_interface.0.ip_address
#}

#output "external_ip_address_vm_3" {
#  value = yandex_compute_instance.vm-3.network_interface.0.nat_ip_address
#}

#output "web_loadbalancer_url" {
#  value = yandex_lb_network_load_balancer.lb-tera.id
#}


#resource "yandex_vpc_network" "default" {}

resource "yandex_dns_zone" "zone1" {
  name        = "my-private-zone"
  description = "desc"

  labels = {
    label1 = "label-1-value"
  }

  zone             = "myfirstdevops.ru."
  public           = true
}


resource "yandex_dns_recordset" "site-1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "test.myfirstdevops.ru."
  type    = "A"
  ttl     = 200
  data    = [(yandex_lb_network_load_balancer.lb-tera.listener[*].external_address_spec[*].address)[0][0]]
}

# resource "yandex_dns_recordset" "site-2" {
#  zone_id = yandex_dns_zone.zone1.id
#  name    = "a.ask555l.example.com."
#  ttl     = 600
#  type    = "ANAME"
#  data    = ["ask555l.example.com.website.yandexcloud.net"]
#}
