variable "yandex_token" {
    description = "token for yandex cloud"
    type = string
    default = ""
}
variable "yandex_cloud_id" {
    description = "yandex cloud id"
    type = string
    default = ""
}
variable "yandex_folder_id" {
    description = "yandex folder id"
    type = string
    default = ""

